<?php

/**
 * @file
 * Administration pages provided by Webform tooltip module.
 */

/**
 * Menu callback for admin/config/content/webform_tooltip.
 */
function webform_tooltip_admin_settings_form() {
  $form['webform_tooltip_icon_text'] = array(
    '#type' => 'textfield',
    '#title' => t('Icon text'),
    '#default_value' => variable_get('webform_tooltip_icon_text', '?'),
    '#required' => TRUE,
  );
  return system_settings_form($form);
}
