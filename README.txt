INTRODUCTION
------------

This module allows you to show your webform component's description as a tooltip
style on mouse hover event.


REQUIREMENTS
------------

This module requires the following module:
 * Webform (https://www.drupal.org/project/webform)


INSTALLATION
------------

* Install as you would normally install a contributed Drupal module. See:
  https://drupal.org/documentation/install/modules-themes/modules-7
  for further information.


CONFIGURATION
-------------

* To configure the icon text go to admin/config/content/webform_tooltip


USAGE
-----

* In the webform component's edit page:
  - enter a description for the component
  - select "Show the description as a tooltip"
  - choose the "Tooltip Icon" : prefix or suffix
