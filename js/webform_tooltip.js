/**
 * @file
 * JS file for webform_tooltip module.
 */

(function ($) {

  "use strict";

  Drupal.webform_tooltip = Drupal.webform_tooltip || {};

  Drupal.behaviors.webform_tooltip_icon = {
    attach: function (context, settings) {
      var icon = "<a class='vtip webform-tooltip-icon'>"+settings.webform_tooltip_icon.text+"</a>";
      $('.webform-tooltip-suffix', context).once('webform_tooltip_icon').append(icon);
      $(icon, context).once('webform_tooltip_icon').insertAfter(jQuery(".webform-tooltip-prefix").find('label:first'));
      Drupal.webform_tooltip.show_tooltip();
    }
  };

  Drupal.webform_tooltip.show_tooltip = function () {
    var xOffset = -5;
    var yOffset = 25;
    jQuery(".vtip").unbind().hover(function (a) {
      this.t = jQuery(this).closest('div.webform-tooltip').attr('tip_description');
      this.top = (a.pageY + yOffset);
      this.left = (a.pageX + xOffset);
      jQuery("body").append('<p id="vtip">' + this.t + "</p>");
      jQuery("p#vtip #vtipArrow");
      jQuery("p#vtip").css("top", this.top + "px").css("left", this.left + "px").fadeIn("slow");
    }, function () {
      jQuery("p#vtip").fadeOut("slow").remove();
    }).mousemove(function (a) {
      this.top = (a.pageY + yOffset);
      this.left = (a.pageX + xOffset);
      jQuery("p#vtip").css("top", this.top + "px").css("left", this.left + "px");
    });
  };

})(jQuery);
